/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mensajes_app;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Arnold
 */
public class Conexion {
    
    Connection conectar = null;
    String usuario = "root";
    String contrasenya = "";
    String bd = "mensajes_app";
    String ip = "localhost"; 
    String puerto = "3306";
    
    String cadena = "jdbc:mysql://"+ip+":"+puerto+"/"+bd;
    
    public Connection establecerConexion(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conectar = DriverManager.getConnection(cadena, usuario, contrasenya);
            System.out.println("Conexión satisfactoria: ESTÁ CONECTADO");
            
        }catch(Exception e){
            System.out.println("No se pudo establecer la conexión! ERROR: "+ e.toString());
        }
        
        return conectar;
    }
    
}
